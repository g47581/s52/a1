// const validLetter = 'o';
// const invalidLetter = 'abc';
// const sentence = 'The quick brown fox jumps over the lazy dog'


function countLetter(letter, sentence) {
    let result = 0;

    

    // If letter is invalid, return undefined.
    if(letter.length    > 1){
        return  undefined   
    }else {
        for(let i = 0; i < sentence.length; i++){
            if(sentence [i] == letter){
               result++      
            }
        }
        return result
    }
    
    //Conditons:
        // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        // If letter is invalid, return undefined.
    
    
}
// const result = countLetter(invalidLetter  , sentence  )
// console.log(result)


// const isogram = 'Machine';
// const notIsogram = 'Hello';

function isIsogram(text) {

    // console.log(text)
    //Goal:
        // An isogram is a word where there are no repeating letters.

    //Check:
        // The function should disregard text casing before doing anything else.
    text.toLowerCase()


    let letters = []
    // console.log(letters.indexOf("c"))

    for (let i = 0; i < text.length; i++){
        //Condition:
        if(letters.indexOf(text[i]) !== -1){

            // If the function finds a repeating letter, return false. Otherwise, return true.
            return false
        }else{
            letters.push(text[i])
        }
    }
    return true

}
// const result = isIsogram(isogram);
// console.log(result)
// const result = isIsogram(notIsogram)
// console.log(result)

const price = 109.4356;
const discountedPrice = price * 0.8;
const roundedPrice = discountedPrice.toFixed(2);

function purchase(age, price) {
        //Conditions:        
        // Return undefined for people aged below 13.
        if(age < 13){
                return undefined
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        }else if (age >= 13 && age <= 21 || age > 65)
              {return  (price * 0.8).toFixed(2)
        }  else {
            return  price.toFixed(2)
        }
    //Check:
        // The returned value should be a string.
    
}

// const result = purchase(12, price)

// const result = purchase(15, price)
//const result = purchase(72, price)
// const result = purchase(34, price)
// console.log(typeof result, result)



// const items = [
//     { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
//     { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
//     { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
//     { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
//     { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
// ];

function findHotCategories(items) {

    console.log(items)
    //Goal:
        // Find categories that has no more stocks.
        // The hot categories must be unique; no repeating categories.

    //Expected return must be array:
        // The expected output after processing the items array is ['toiletries', 'gadgets'].

    // Note:
        // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    let hotCategories = []
    items.forEach(item => {
        if(item.stocks == 0){
            if(hotCategories.indexOf(item.category)== -1){hotCategories .push(item.category)

            }
        }
    })
    return hotCategories     
}

// const result = findHotCategories(items)
// console.log(result)



// const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
// const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

function findFlyingVoters(candidateA, candidateB) {


    // console.log(candidateA)
    // console.log(candidateB)
    //Goal:
        // Find voters who voted for both candidate A and candidate B.

    

    // Expected return must be array:
        // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

    //Note:
        // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.


    let flyingVoters = []
    candidateA.forEach(voterA =>{
        if(candidateB.indexOf(voterA) !== -1){
            flyingVoters.push(voterA)

        }
    })
    return  flyingVoters
}


// const result = findFlyingVoters(candidateA, candidateB)

// console.log(result)

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};